DIR_PROJECT=$HOME/Sheikh\ Saad\ Al-Gamdi
DIR_DOWNLOAD=$HOME/audio/Ghamadi_40kbps
for file in $(ls $DIR_DOWNLOAD)
do
    files=$(echo "${file%}" | sed 's/.mp3//g' | awk 'BEGIN{FS=""}{for(i=NF/2+1;i<=NF;i++){printf $i}{printf "\n"}}' | sed 's/^0*//')
    dirs=$(echo "${file%}" | sed 's/.mp3//g' | awk 'BEGIN{FS=""}{for(i=1;i<=NF/2;i++)printf $i}{printf "\n"}' | sed 's/^0*//')
    cari=$(find . -name "$dirs" -type d)
    if [[ $cari ]]
    then
	cp "$DIR_DOWNLOAD/$file" "$DIR_PROJECT/$dirs/$files.mp3"
	echo "success copy: $dirs/$files.mp3"
    else
	mkdir "$DIR_PROJECT/$dirs"
	cp "$DIR_DOWNLOAD/$file" "$DIR_PROJECT/$dirs/$files.mp3"
	echo "success copy: $dirs/$files.mp3"
    fi
done
